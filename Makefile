.PHONY: twemojis, twemojis-extra, byo-twemojis, clean, all

twemojis:
	./src/twemojis/generate.py
	./src/package.sh twemojis

twemojis-extra:
	./src/twemojis-extra/generate.py
	./src/package.sh twemojis-extra

byo-twemojis:
	./src/byo-twemojis/generate.py
	./src/package.sh byo-twemojis

clean-twemojis:
	rm -rf packages/pdf-twemojis
	rm -f packages/twemojis.*
	rm -rf dist/twemojis
	rm -f dist/twemojis.zip

clean-byo-twemojis:
	rm -f packages/byo-twemojis.*
	rm -rf dist/byo-twemojis
	rm -f dist/byo-twemojis.zip

clean:
	rm -rf packages
	rm -rf dist

all: clean twemojis byo-twemojis twemojis-extra
