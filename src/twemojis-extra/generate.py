#!/usr/bin/env python3
"""Converts the SVGs in the src/twemoji folder to PDF and generates the .ins and .dtx files."""

import json
import os
import shutil
import subprocess
from datetime import date
from operator import itemgetter
from pathlib import Path

from jinja2 import Template

VERSION = "0.1-alpha"
AUTHOR = "Jost Rossel"

JINJA_SYNTAX = {
    "block_start_string": "<%",
    "block_end_string": "%>",
    "variable_start_string": "<<",
    "variable_end_string": ">>",
    "comment_start_string": "<#",
    "comment_end_string": "#>",
}

script_folder = os.path.dirname(os.path.realpath(__file__))
git_root_dir = os.path.abspath(os.path.join(script_folder, "..", ".."))
source_folder = os.path.join(git_root_dir, "src")
svg_source_folder = os.path.join(source_folder, "twemojis-extra", "assets")
target_folder = os.path.join(git_root_dir, "packages")
pdf_temp_folder = os.path.join(git_root_dir, "packages", "pdf-extra-twemojis")
all_twemojis_pdf = os.path.join(git_root_dir, "packages", "all-extra-twemojis.pdf")

# ensure that all target folders exist and pdf_temp_folder is empty
Path(pdf_temp_folder).mkdir(parents=True, exist_ok=True)
shutil.rmtree(pdf_temp_folder)
Path(pdf_temp_folder).mkdir(parents=True, exist_ok=True)

with open(os.path.join(script_folder, "aliases.json"), encoding="utf-8") as alias_file:
    EMOJI_ALIASES = json.load(alias_file)


def convert_svg_to_pdf():
    """Converts the SVG files to PDF using inkscape.
    Returns all filenames, their list index corresponds to the page in the PDF (+1)."""
    names = set()
    inkscape_call = []

    targets = set()
    for _filename in os.listdir(svg_source_folder):
        if _filename[-4:] != ".svg":
            continue
        names.add(_filename[:-4])
        tar = f"{os.path.join(pdf_temp_folder, _filename[:-4])}.pdf"
        inkscape_call.append(
            f"file-open:{os.path.join(svg_source_folder, _filename)};"
            "export-type:pdf;"
            f"export-filename:{tar};"
            "export-do;"
        )
        targets.add(tar)
    inkscape_call.append("\n")

    subprocess.run(
        ["inkscape", "--shell"],
        input="".join(inkscape_call).encode("utf-8"),
        check=True,
    )

    # verify generation
    for tar in targets:
        if not os.path.isfile(tar):
            print("Missing", tar)

    names = sorted(names)
    sorted_targets = [f"{os.path.join(pdf_temp_folder, name)}.pdf" for name in names]

    subprocess.run(
        ["pdfunite", *sorted_targets, all_twemojis_pdf],
        check=True,
    )

    return names


def get_metadata():
    """Retrieves the needed metadata for the templates."""
    metadata = {}
    metadata["year"] = date.today().year
    metadata["month"] = date.today().month
    metadata["day"] = date.today().day
    metadata["version"] = VERSION
    metadata["author"] = AUTHOR
    return metadata


if __name__ == "__main__":
    filenames = convert_svg_to_pdf()

    context = get_metadata()

    context["emojinames"] = []
    context["emojinames_mapping"] = {}
    for index, filename in enumerate(filenames):
        page = index + 1
        aliases = [filename]
        if filename in EMOJI_ALIASES:
            aliases.extend(EMOJI_ALIASES[filename])
        context["emojinames_mapping"][filename] = aliases
        for alias in aliases:
            context["emojinames"].append((page, alias))
    context["emojinames"].sort(key=itemgetter(0, 1))
    context["emojinames_mapping"] = sorted(context["emojinames_mapping"].items())

    with open(os.path.join(git_root_dir, "src", "licenses.tex"), encoding="utf-8") as f:
        context["license_text"] = f.read().split("\n")

    with open(os.path.join(target_folder, "twemojis-extra.ins"), "w", encoding="utf-8") as outfile:
        ins_template = Template(
            open(os.path.join(script_folder, "ins.jinja"), encoding="utf-8").read(), **JINJA_SYNTAX
        )
        outfile.write(ins_template.render(context))

    with open(os.path.join(target_folder, "twemojis-extra.dtx"), "w", encoding="utf-8") as outfile:
        dtx_template = Template(
            open(os.path.join(script_folder, "dtx.jinja"), encoding="utf-8").read(), **JINJA_SYNTAX
        )
        outfile.write(dtx_template.render(context))
