# Twemojis Extra

Additional graphics for the `twemojis` package.
The `twemojis` package is required (and loaded automatically when loading this package).
The emojis are registered internally with the commands provided by the `twemojis` package.
Hence, both the `\twemoji` and `\texttwemoji` commands work exactly the same.

As the provided graphics are intended to be used in presentations (i.e. on a larger scale) I focused on them looking good on said larger scale.
Some graphics are too detailed for a small-scale usage.
Some graphics in this package are simple adaptations of existing twemojis.

Overall, please keep in mind, that I am not a designer and just trying my best.
If you want to help with the designs or really need a specific graphic please contact me (best via the issue tracker at \url{https://gitlab.com/rossel.jost/latex-twemojis/-/issues}).
