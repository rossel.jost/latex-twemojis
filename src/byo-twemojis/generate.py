#!/usr/bin/env python3
"""Converts the SVGs in the twemoji folder to partial tikz images"""

import glob
import os
from datetime import date
from pathlib import Path

from jinja2 import Template

from svg2tikz import convert_svg

VERSION = "1.0"
AUTHOR = "Jost Rossel"

JINJA_SYNTAX = {
    "block_start_string": "<%",
    "block_end_string": "%>",
    "variable_start_string": "<<",
    "variable_end_string": ">>",
    "comment_start_string": "<#",
    "comment_end_string": "#>",
}

script_folder = os.path.dirname(os.path.realpath(__file__))
source_files = glob.glob(os.path.join(script_folder, "elements", "*.svg")) + glob.glob(
    os.path.join(script_folder, "elements", "**", "*.svg")
)
git_root_dir = os.path.join(script_folder, "..", "..")
target_folder = os.path.join(git_root_dir, "packages")

# ensure that all target folders exist
Path(target_folder).mkdir(parents=True, exist_ok=True)

if __name__ == "__main__":
    context = {
        "elements": {},
        "emojis": [
            (
                "1f600",
                "head; eyes normal; mouth laughing",
                "",
            ),
            (
                "1f601",
                "head; eyes smiling; mouth grinning",
                "",
            ),
            (
                "1f602",
                "head; expression laughing; mouth laughing; laughing tears",
                "",
            ),
            (
                "1f603",
                "head; eyes normal opened; mouth laughing",
                "",
            ),
            (
                "1f604",
                "head; eyes closed laughing; mouth laughing",
                "",
            ),
            (
                "1f605",
                "head; eyes closed laughing; mouth laughing; sweat upper right",
                "",
            ),
            (
                "1f606",
                "head; eyes tightly closed pupil; mouth laughing",
                "",
            ),
            (
                "1f607",
                "head; eyes smiling!yshift=1.2; mouth smiling high!yshift=1.2; halo",
                "",
            ),
            (
                "1f608",
                "horns; head; eyes normal low; eyebrows angry!yshift=2.2; mouth smiling!yshift=2",
                "let Yellow=byoTwemojiLightPurple, let Brown=byoTwemojiPurple",
            ),
            (
                "1f609",
                "head; eyes winking; eyebrows winking; mouth smiling small",
                "",
            ),
            (
                "1f60a",
                "head; blush!yshift=-3; eyes slightly smiling; mouth smiling high",
                "",
            ),
            (
                "1f60b",
                "head; eyes slightly smiling wide; mouth smiling tongue",
                "",
            ),
            (
                "1f60c",
                "head; expression relieved; mouth smiling small",
                "",
            ),
            (
                "1f60d",
                "head; eyes hearts; mouth smiling large",
                "",
            ),
            (
                "1f60e",
                "head; sunglasses; mouth smiling",
                "",
            ),
            (
                "1f60f",
                "head; expression smirking; mouth smirking",
                "",
            ),
            (
                "1f610",
                "head; eyes normal!yshift=1.5; mouth neutral",
                "",
            ),
            (
                "1f611",
                "head; eyes neutral; mouth neutral",
                "",
            ),
            (
                "1f612",
                "head; expression unamused; mouth frowning",
                "",
            ),
            (
                "1f613",
                "head; eyes closed laughing; mouth frowning small; sweat upper left",
                "",
            ),
            (
                "1f614",
                "head; eyes closed downcast; mouth neutral small; eyebrows worried!yshift=1",
                "",
            ),
            (
                "1f615",
                "head; eyes normal!yshift=1.5; mouth half frowning",
                "",
            ),
            (
                "1f616",
                "head; eyes tightly closed pupil!yshift=2; eyebrows worried!yshift=1; mouth confounded",
                "",
            ),
            (
                "1f617",
                "head; eyes normal low!yshift=-4; mouth kissing",
                "",
            ),
            (
                "1f618",
                "head; eyes winking; eyebrows winking; mouth kissing; heart at mouth",
                "let Shadow=byoTwemojiDarkYellow, set ShadowTransparency=.9",
            ),
            (
                "1f619",
                "head; eyes smiling; mouth kissing",
                "",
            ),
            (
                "1f61a",
                "head; blush; eyes closed; eyebrows normal; mouth kissing",
                "",
            ),
            (
                "1f61b",
                "head; eyes normal; mouth stuck-out tongue",
                "",
            ),
            (
                "1f61c",
                "head; eyes winking large; mouth stuck-out tongue",
                "",
            ),
            (
                "1f61d",
                "head; eyes tightly closed pupil; mouth stuck-out tongue",
                "",
            ),
            (
                "1f61e",
                "head; eyes disappointed; mouth frowning small",
                "",
            ),
            (
                "1f61f",
                "head; eyes normal!yshift=1.5; eyebrows worried; mouth frowning small",
                "",
            ),
            (
                "1f620",
                "head; eyes normal low!yshift=-1; eyebrows angry!yshift=1.8; mouth frowning!yshift=2",
                "",
            ),
            (
                "1f621",
                "head; eyes normal low!yshift=-1; eyebrows angry!yshift=1.8; mouth frowning!yshift=2",
                "let Yellow=byoTwemojiRed, let Brown=byoTwemojiBlack",
            ),
            (
                "1f622",
                "head; eyes normal!yshift=1.5; eyebrows normal; mouth frowning small; tear left",
                "",
            ),
            (
                "1f623",
                "head; eyes tightly closed; eyebrows worried!yshift=-1.8; mouth frowning open small",
                "",
            ),
            (
                "1f624",
                "head; expression boastful; mouth frowning deep; exhaling",
                "",
            ),
            (
                "1f625",
                "head; eyes normal!yshift=1.5; eyebrows worried; mouth frowning small; sweat lower left",
                "",
            ),
            (
                "1f626",
                "head; eyes normal!yshift=1.5; mouth frowning small",
                "",
            ),
            (
                "1f627",
                "head; eyes normal!yshift=1.5; eyebrows normal!yshift=-1.5; mouth frowning small",
                "",
            ),
            (
                "1f628",
                "head; cold sweat; eyes normal!yshift=1.5; eyebrows normal!yshift=-1.5; mouth frowning small!yshift=-1",
                "",
            ),
            (
                "1f629",
                "head; expression weary; mouth frowning open wide",
                "",
            ),
            (
                "1f62a",
                "head; expression sleepy; mouth frowning small; snot",
                "",
            ),
            (
                "1f62b",
                "head; eyes tightly closed!yshift=-2.5; eyebrows worried!yshift=-1.5; mouth frowning open",
                "",
            ),
            (
                "1f62c",
                "head; eyes normal; mouth grimacing",
                "",
            ),
            (
                "1f62d",
                "head; tear stream; expression crying; mouth crying",
                "",
            ),
            (
                "1f62e",
                "head; eyes normal; mouth open wide",
                "",
            ),
            (
                "1f62f",
                "head; eyes normal; eyebrows normal!yshift=-2; mouth open",
                "",
            ),
            (
                "1f630",
                "head; cold sweat; eyes normal!yshift=1.5; eyebrows worried; mouth frowning small; sweat lower left",
                "",
            ),
            (
                "1f631",
                "head; cold sweat; hands shocked; eyes wide open empty; mouth open wide screaming",
                "",
            ),
            (
                "1f632",
                "head; eyes normal; eyebrows normal!yshift=-2; mouth open wide dropped jaw",
                "",
            ),
            (
                "1f633",
                "head; blush; eyes wide open; eyebrows normal!yshift=-2; mouth neutral small",
                "",
            ),
            (
                "1f634",
                "head; eyes closed relaxed; mouth open sleeping; zzz",
                "let Shadow=byoTwemojiDarkYellow, set ShadowTransparency=1",
            ),
            (
                "1f635",
                "head; eyes xx; eyebrows normal!yshift=-2; mouth open wide",
                "",
            ),
            (
                "1f636",
                "head; eyes normal!yshift=1.5;",
                "",
            ),
            (
                "1f637",
                "head; eyes heavy; mask",
                "",
            ),
            (
                "1f641",
                "head; eyes normal; mouth frowning",
                "",
            ),
            (
                "1f642",
                "head; eyes normal; mouth smiling!scale=.8,xshift=3.6,yshift=4.5",
                "",
            ),
            (
                "1f643",
                "head; eyes normal; mouth smiling",
                "rotate around={180:(18,18)}",
            ),
            (
                "1f644",
                "head; eyes rolling; mouth half frowning deep",
                "",
            ),
            (
                "1f910",
                "head; eyes normal; mouth zipped",
                "",
            ),
            (
                "1f911",
                "head; eyes money; mouth stuck-out tongue money",
                "",
            ),
            (
                "1f912",
                "head; eyes normal!yshift=1.5; eyebrows worried; mouth thermostat",
                "",
            ),
            (
                "1f913",
                "head; eyes normal small; glasses; mouth buckteeth",
                "",
            ),
            (
                "1f914",
                "head; expression thinking; mouth thinking; hands thinking",
                "",
            ),
            (
                "1f915",
                "head; bandage; eyes closed laughing; mouth frowning small",
                "",
            ),
            (
                "1f917",
                "head!top centered scale=32; eyes closed laughing raised; mouth smiling raised; hands hugging",
                "",
            ),
            (
                "1f920",
                "head!bottom centered scale=30; eyes normal low!scale=.9,xshift=1.8,yshift=1; mouth smiling!scale=.8,xshift=3.6,yshift=7.2; cowboy hat",
                "",
            ),
            (
                "1f921",
                "head; clown makeup; clown nose; clown hair; mouth smiling!yscale=1.2,yshift=-3,fill=byoTwemojiRed; eyes normal!yshift=-2",
                "set Yellow={254, 231, 184}",
            ),
            (
                "1f922",
                "head; eyes sick; mouth sick",
                "let Yellow=byoTwemojiLightGreen",
            ),
            (
                "1f924",
                "head; eyes closed laughing; eyebrows enjoying; mouth watering",
                "",
            ),
            (
                "1f925",
                "head!left centered scale=32; eyes normal!left centered scale=32,yshift=-1.2; pinocchio nose; mouth half frowning!left centered scale=32,yshift=2.2",
                "",
            ),
            (
                "1f927",
                "head; eyes tightly closed pupil!yshift=-.5; mouth half frowning!xshift=3.5,yshift=1; tissue",
                "",
            ),
            (
                "1f928",
                "head; mouth neutral; expression skeptical",
                "",
            ),
            (
                "1f929",
                "head; eyes stars; mouth laughing",
                "",
            ),
            (
                "1f92a",
                "head; eyes oO; mouth goofy",
                "",
            ),
            (
                "1f92b",
                "head; eyes normal small; eyebrows normal!yshift=-2; mouth open oval; hands shh",
                "",
            ),
            (
                "1f92c",
                "head; expression angry; censor bar",
                "let Yellow=byoTwemojiRed, let Brown=byoTwemojiBlack",
            ),
            (
                "1f92d",
                "head!top centered scale=34; eyes closed laughing!yshift=-3; hands yawning!xshift=3",
                "",
            ),
            (
                "1f92e",
                "head!top centered scale=34; eyes tightly closed pupil; mouth puking",
                "",
            ),
            (
                "1f92f",
                "head exploding; eyes normal low small; mouth open wide!yshift=3",
                "",
            ),
            (
                "1f970",
                "head; eyes smiling!yshift=-.5; mouth smiling!yshift=-2.5; hearts around head",
                "let Shadow=byoTwemojiDarkYellow, set ShadowTransparency=.9",
            ),
            (
                "1f971",
                "head; eyes slightly smiling raised; mouth open wide yawning; hands yawning",
                "",
            ),
            (
                "1f972",
                "head; eyes normal; mouth smiling; tear right",
                "",
            ),
            (
                "1f973",
                "head!bottom left alined scale=34; eyes slightly smiling skewed; mouth party horn; party hat; confetti",
                "",
            ),
            (
                "1f974",
                "head; expression uneven; mouth wavy",
                "",
            ),
            (
                "1f975",
                "head; eyes normal!yshift=1.5; eyebrows worried; mouth tongue hanging-out; sweat left right",
                "let Yellow=byoTwemojiLightRed,let Brown=byoTwemojiBlack",
            ),
            (
                "1f976",
                "head; eyes normal; eyebrows worried raised; mouth grimacing; icicles upper; icicles lower",
                "let Yellow=byoTwemojiBlue, let Brown=byoTwemojiDarkBlue",
            ),
            (
                "1f978",
                "head; eyes normal small; silly disguise",
                "",
            ),
            (
                "1f97a",
                "head; eyes pleading; mouth frowning slightly",
                "",
            ),
            (
                "2639",
                "head; eyes normal; mouth frowning deeply",
                "",
            ),
            (
                "263a",
                "head; blush; expression satisfied; mouth smiling small",
                "",
            ),
        ],
        "colors": [],
        "version": VERSION,
        "author": AUTHOR,
        "year": date.today().year,
        "month": date.today().month,
        "day": date.today().day,
    }
    color_substitutions = {
        "White": ["f5f8fa", "ffffff", "f4f7f9", "fff"],
        "WhiteGray": ["e1e8ed", "e6e7e8"],
        "LightGray": ["ccd6dd", "d1d3d4"],
        "Gray": ["99aab5", "bcbec0"],
        "DarkGray": ["67757f"],
        "Black": ["292f33", "000"],
        "LightBrown": ["825d0e"],
        "Brown": ["664500", "65471b", "66471b"],
        "Yellow": ["ffcc4d", "ffcb4c"],
        "DarkYellow": ["fcab40", "ffac33"],
        "LightOrange": ["f19020", "f4900c"],
        "Orange": ["f36c24", "e95f28"],
        "DarkOrange": ["b55005", "c1694f"],
        "LightRose": ["f4abba"],
        "Rose": ["ff7892"],
        "LightRed": ["e75b70", "e75a70", "e8596e", "ea596e"],
        "Red": ["de3146", "dd2e44", "dd2f45"],
        "DarkRed": ["da2f47"],
        "WineRed": ["bb1a34"],
        "DarkWineRed": ["662113"],
        "LightBlue": ["bdddf4", "88c9f9", "55acee", "bbddf5"],
        "Blue": ["5dadec", "3b88c3"],
        "DarkBlue": ["2a6797", "4289c1", "226699", "1c6399", "269"],
        "LightGreen": ["77af57", "77b255"],
        "Green": ["5d8f3f", "5d9040"],
        "DarkGreen": ["3e721d"],
        "LightPurple": ["aa8dd8"],
        "Purple": ["553986"],
        "Shadow": ["000000"],
    }
    COLOR_PREFIX = "byoTwemoji"

    for name, colors in color_substitutions.items():
        _rgb = tuple(int(colors[0][i : i + 2], 16) for i in (0, 2, 4))
        context["colors"].append((f"{COLOR_PREFIX}{name}", name, _rgb))

    for filename in source_files:
        code = convert_svg(
            filename,
            verbose=True,
            codeoutput="codeonly",
        )
        code = (
            code.replace("\n", "")
            .replace("]", ", dynamic style/.expand once=#1]")
            .replace(
                "fill opacity=0.300",
                f"fill={COLOR_PREFIX}Shadow, opacity=\\byoTwemojiShadowTransparency",
            )
        )
        for name, colors in color_substitutions.items():
            for color in colors:
                for variantion in [color.upper(), color.lower()]:
                    code = code.replace(f"fill=c{variantion}", f"fill={COLOR_PREFIX}{name}")
        emoji_name = filename[:-4].replace("_", " ").replace("/", " ")
        emoji_name = emoji_name.split(" elements ")[1]
        context["elements"][emoji_name] = code

    context["element_keys_pairs"] = zip(
        sorted(context["elements"].keys())[::2], sorted(context["elements"].keys())[1::2]
    )

    with open(os.path.join(git_root_dir, "src", "licenses.tex")) as f:
        context["license_text"] = f.read().split("\n")

    with open(os.path.join(target_folder, "byo-twemojis.ins"), "w") as outfile:
        ins_template = Template(
            open(os.path.join(script_folder, "ins.jinja")).read(), **JINJA_SYNTAX
        )
        outfile.write(ins_template.render(context))

    with open(os.path.join(target_folder, "byo-twemojis.dtx"), "w") as outfile:
        dtx_template = Template(
            open(os.path.join(script_folder, "dtx.jinja")).read(), **JINJA_SYNTAX
        )
        outfile.write(dtx_template.render(context))
