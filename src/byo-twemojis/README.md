# Build You Own Twemojis

This package allows you to create your own twemojis (the simple round, yellow ones).
The package provides the command `\byoTwemoji` which creates a Ti*k*Z picture based on the elements (eyes, mouth, ...) given to the command.

This package is based on [twemoji 13.0.1](https://github.com/twitter/twemoji/releases/tag/v13.0.1).
