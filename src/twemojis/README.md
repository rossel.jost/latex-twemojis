# Twemojis

Simple wrapper that allows to use Twitter's emojis through LaTeX commands.
As this package works with graphics and not with fonts to achieve its goal, it *should* work on every machine with any LaTeX version without problems.

In comparison, the `emoji` package on CTAN <https://www.ctan.org/pkg/emoji> works on fonts and requires lualatex.
It is lighter and probably compiles faster if you use thousands of emojis, but it also has stricter requirements and is not as flexible.

This package really is intended to be used within Ti*k*Z images.

## Usage

This package in on [CTAN](https://www.ctan.org) as [twemojis](https://ctan.org/pkg/twemojis), so you should be able to use it like any other package, with

```latex
\usepackage{twemojis}
```

An emoji can be used via `\twemoji{...}`.
There is the alternative `\texttwemoji{...}` command which scales and sets the emoji to fit the text line.
More information can be found in the [package's documentation](https://ftp.gwdg.de/pub/ctan/macros/latex/contrib/twemojis/twemojis.pdf).

## Installation

If the package is not in you TeX distribution try [these install steps](https://tex.stackexchange.com/questions/73016/how-do-i-install-an-individual-package-on-a-linux-system).
The TL;DR for the manual installation (if everything else fails) is:
- Run `kpsewhich -var-value TEXMFLOCAL` and `kpsewhich -var-value TEXMFHOME` and check which of the resulting directories is populated.
  Lets call the resulting directory `<base dir>`.
- Create the directory `<base dir>/tex/latex/twemojis` and move the `.sty` file and the folder `pdf-twemojis` there.
- Run `mktexlsr` (you probably need root for that).
- If something doesn't work, read the long version linked above (Method 3).

## Problems/TODOs

- Currently the emojis are not text-selectable, that might change in future versions.

# Implementation Notice

As the emojis are PDF-based and use transparency, they include groups.
I was not able to change the `inkscape` export so that that does not happen.
Hence, `pdflatex` throws a warning about multiple groups on one PDF page.
This package disables said warning: `\pdfsuppresswarningpagegroup=1` as it is of no concern for the inputted emojis.
It **could** happen, that a PDF you input is faulty and you don't notice due to said suppression.
You can enable the warning with `\pdfsuppresswarningpagegroup=0`.
