# LaTeX Twemojis

This project is the source for multiple LaTeX packages regarding Twitter's emojis (twemojis).
These packages where created from the urge to use emojis in presentations, especially in the context of Alice-Bob-style protocols and graphics from the cryptography and the broader IT (security) scope.

- [twemojis](src/twemojis) is a simple wrapper that allows to use Twitter's emojis through LaTeX commands. It is particularly useful in Ti*k*Z images, as it is not based on fonts.
   - [CTAN](https://ctan.org/pkg/twemojis)
   - [documentation](https://ctan.net/macros/latex/contrib/twemojis/twemojis.pdf)
- [twemojis-extra](src/twemojis-extra) contains variants of the official twemojis and self-made emojis of a similar style.
  - *not yet published on CTAN*
- [byo-twemojis](src/byoTwemojis) (**b**uild **y**our **o**wn twemojis) allows you to create custom twemojis (only the “classical” ones with the round head). This package is based on Ti*k*Z images.
   - [CTAN](https://www.ctan.org/pkg/byo-twemojis)
   - [documentation](https://ctan.net/graphics/pgf/contrib/byo-twemojis/byo-twemojis.pdf)

# Development

## Building

To build the packages you need `python 3` and a PDF-capable LaTeX installation (`pdflatex`, `lualatex`, ...).
For the `twemojis` and the `twemojis-extra` packages you also need `inkscape` and `pdfunite` (poppler), and for `twemojis` the official Twemoji Repo (init the submodule).

This project uses [poetry](https://python-poetry.org/), so either use that or install the packages from the `pyproject.toml` manually.

To create the ZIP file that is uploaded to CTAN simply run `make <project-name>`.
The resulting ZIP is in the `./dist` folder, the contents of the ZIP files are in sibling-folders.
The `.sty` and compiled documentation can be found in `./packages`.
Both `./dist` and `./packages` are ignored by Git.

# Licenses

## Emojis

The emojis and all derived graphics belong to Twitter, Inc and other contributors (Copyright 2019).
They are licensed under CC-BY 4.0: https://creativecommons.org/licenses/by/4.0/.

Hence, attribution of the original work is needed.

### Attribution

**I'm no lawyer, so take this section with a grain of salt.**

As the emojis themselves are licensed under CC-BY they require attribution but are open to be distributed and modified anyway you like (which makes these packages possible).
I'm not sure whether the attribution in the package's source code and in this repository is enough for a file (e.g. PDF) generated with this package to be covered as well (doubt it, really).

So, I see two different possibilities to attribute the emojis in a compiled document:

1. Add attribution to the metadata of the document.
   For example with <https://www.ctan.org/pkg/hyperxmp> in PDF files.
2. Add attribution in the document directly (e.g. on the last page of a presentation).
   For example:
   ```
   Emoji graphics licensed under CC-BY 4.0: https://creativecommons.org/licenses/by/4.0/
   Copyright 2019 Twitter, Inc and other contributors
   ```
   I consider this option to be the safest.

## LaTeX Package

The LaTeX packages are licensed under the LPPL 1.3 or later License.

> Copyright (c) 2021-2022 Jost Rossel
> This file may be distributed and/or modified under the
> conditions of the LaTeX Project Public License, either
> version 1.3 of this license or (at your option) any later
> version. The latest version of this license is in:
>
>     http://www.latex-project.org/lppl.txt
>
> and version 1.3 or later is part of all distributions of
> LaTeX version 2005/12/01 or later.

## Python Code

The Python code that generates the LaTeX packages is licensed under the MIT License.

> Copyright (c) 2021-2022 Jost Rossel
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
